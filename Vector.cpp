//#include "vector.hpp"
#include <iostream>
#include <stdexcept>
#include "Vector.h"

using Value = double;

void Vector::addMemory(size_t count)
{
    if (_size >= _capacity)
    {
    	if (_capacity == 0)
        {
    	    _capacity = 1;
		}
      
        _capacity *= _multiplicativeCoef;
        Value* tmp = new Value[_capacity];
		
        for(size_t i = 0; i < _size - count; i++)
        {
            tmp[i]=std::move(_data[i]);
        }
		
        delete[] _data;
        _data = tmp;
    }
}

Vector::Vector(const Value* rawArray, const size_t size, float coef)
{
	_size = size;
	_capacity = size;
	_multiplicativeCoef = coef;
	_data = new Value[_capacity];
	
	for (size_t i = 0; i < _size; i++)
	{
		_data[i] = rawArray[i];
	}
}

Vector::Vector(const Vector& other)
{
	_size = other._size;
	_capacity = other._size;
	_multiplicativeCoef = other._multiplicativeCoef;
	_data = new Value[_size];
	
	for (size_t i = 0; i < _size; i++)
	{
		_data[i] = other._data[i];
	}
}

Vector& Vector::operator=(const Vector& other)
{
	if (&other != this)
	{
		this -> ~Vector();
		
		_multiplicativeCoef = other._multiplicativeCoef;
		_size = other._size;
		_capacity = other._capacity;
		_data = new Value[_size];
		
		for (size_t i = 0; i < _size; i++)
		{
			_data[i] = other._data[i];
		}
	}
	
	return *this;
}

Vector::Vector(Vector&& other) noexcept
{
	_data = other._data;
	_size = other._size;
	_capacity = other._capacity;
	_multiplicativeCoef = other._multiplicativeCoef;
	
	other._data = nullptr;
	other._size = 0;
	other._capacity = 0;
	other._multiplicativeCoef = 2.0f;
}

Vector& Vector::operator=(Vector&& other) noexcept
{
	if (&other != this)
	{
		_data = other._data;
		_multiplicativeCoef = other._multiplicativeCoef;
		_capacity = other._capacity;
		
		other._data = nullptr;
		other._capacity = 0;
		_size = other._size;
		other._size = 0;
	}
	
	return *this;
}

Vector::~Vector()
{
	delete[] _data;
	_data = nullptr;
	_size = 0;
	_capacity = 0;
	_multiplicativeCoef = 2.0f;
}


void Vector::pushBack(const Value& value)
{
	if (_size >= _capacity)
	{
    	addMemory(1);
	}
	
    _data[_size] = value;
}

void Vector::pushFront(const Value& value)
{
	_size++;
	
	if (_size >= _capacity)	
	{
		addMemory(1);
	}
	
	for (size_t i = _size-1; i > 0; i--)
	{
		_data[i] = _data[i - 1];
	}
	
	_data[0] = value;
}

void Vector::insert(const Value& value, size_t pos)
{
	if (_size < pos) 
	{
		throw std::invalid_argument("Вы инвалид, некорректные данные");
	}
	
	_size++;
	
	if (_size >= _capacity) 
	{
		addMemory(1);
	}

	for (int i = _size - 1; i > pos; i--)
	{
    	_data[i] = _data[i - 1];
	}
	
	_data[pos] = value;
}


void Vector::insert(const Value* values, size_t size ,size_t pos)
{
    _size += size;
	
    while (_size >= _capacity)
    {
        addMemory(size);
    }
	
    int count = 1;
	
    for (size_t i = _size - 1; i > pos; i--)
    {
        _data[i] = _data[_size - size - count];
	    count++;
    }
	
    for (size_t i = 0; i < size; i++)
    {
    	_data[pos + i] = values[i];
    }
	
}

void Vector::insert(const Vector& vector, size_t pos)
{
	if (_size < pos) 
	{
		throw std::invalid_argument("Вы инвалид, некорректные данные");
	}
	
	insert(vector._data, vector._size, pos);
}

Value& Vector::operator[](size_t idx)
{
	return _data[idx];
}

const Value& Vector::operator[](size_t idx) const
{
	return _data[idx];
}

void Vector::popBack()
{
	if (_size > 0)
	{
    	_size--;
	}
		
    else
	{
		throw std::invalid_argument("Вы инвалид, некорректные данные");
	}
}

void Vector::popFront(){
	if (_size > 0)
	{
		for (size_t i = 0; i < _size - 1; i++)
		{
	        _data[i] = _data[i + 1];
		}
	    _size--;
	}
		
	else 
	{
		throw std::invalid_argument("Вы инвалид, некорректные данные");
	}
}

void Vector::erase(size_t pos, size_t count)
{
	if (_size < pos)
	{
        throw std::invalid_argument("Вы инвалид, некорректные данные");
    }
	
    size_t counter = std::min(count, _size - pos);
	
    for (size_t  i = pos; i < _size - counter;  i++)
	{
        _data[i] = _data[counter + i];
    }
	
    _size -= counter;
}

void Vector::eraseBetween(size_t beginPos, size_t endPos)
{
	if (_size < endPos - beginPos)
	{
    	throw std::invalid_argument("Вы инвалид, некорректные данные");
    }
	
	erase(beginPos, endPos - beginPos);
}

size_t Vector::size() const
{
	return  _size;
}

size_t Vector::capacity() const
{
	return _capacity;
}

double Vector::loadFactor() const
{
	return double(_size) / _capacity;
}

long long Vector::find(const Value&doubl) const
{
	for (size_t i = 0; i < _size; i++)
	{
        if (_data[i] == doubl)
		{
            return i;
        }
    }
    return -1;
}

void Vector::reserve(size_t capacity)
{
	if (capacity <= _capacity)
	{
		return;
	}
	
    Value* tmp = new Value[capacity];
	
    for (size_t i = 0; i < _size; i++)
	{
        tmp[i] = _data[i];
    }
	
    delete[] _data;
    _data = tmp;
    _capacity = capacity;
}

void Vector::shrinkToFit()
{
    Value* tmp = new Value[_size];
	
    for (size_t i = 0; i < _size; i++)
	{
        tmp[i] = _data[i];
    }
	
    delete[] _data;
    _data = tmp;
	
    _capacity = _size;
}

Vector::Iterator::Iterator(Value* ptr)
{
	_ptr = ptr;
}

Value& Vector::Iterator::operator*()
{
    return *this->_ptr;
}

const Value& Vector::Iterator::operator*() const
{
    return *this->_ptr;
}

Vector::Iterator Vector::Iterator::operator++()
{
    _ptr++;
    return *this;
}

Vector::Iterator Vector::Iterator::operator++(int)
{
    Vector::Iterator tmp(_ptr);
    _ptr++;
    return tmp;
}

Value* Vector::Iterator::operator->()
{
    return _ptr;
}

const Value* Vector::Iterator::operator->() const
{
    return _ptr;
}

bool Vector::Iterator::operator==(const Vector::Iterator& other) const
{
    return _ptr == other._ptr;
}

bool Vector::Iterator::operator!=(const Vector::Iterator& other) const
{
    return _ptr != other._ptr;
}

Vector::Iterator Vector::begin()
{
    Vector::Iterator tmp(_data);
    return tmp;
}

Vector::Iterator Vector::end()
{
    Vector::Iterator tmp(_data + _size);
    return tmp;
}

